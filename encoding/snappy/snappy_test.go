package snappy

import (
	"google.golang.org/grpc/encoding"
	"testing"
)

import (
	"bytes"
	"fmt"
)

func TestCompress1(t *testing.T) {
	var cmp = encoding.GetCompressor(Name)

	str := []byte("dfdasfdsafdsaf12323dfasdfdsafadfdsafdsafdasfdsa")
	var buf bytes.Buffer
	z, _ := cmp.Compress(&buf)
	_, e := z.Write(str)
	if e != nil {
		fmt.Printf("err: %s\n", e)
	}

	if err := z.Close(); err != nil {
		fmt.Printf("err: %s\n", err)
	}

	compressData := buf.Bytes()
	size := len(compressData)

	rbytes := make([]byte, size+bytes.MinRead)
	rd := bytes.NewReader(rbytes)
	d, _ := cmp.Decompress(rd)

	d.Read(rbytes)
	fmt.Printf("d: %s\n", string(rbytes))
}
