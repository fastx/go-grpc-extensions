package zstd

import (
	"errors"
	"github.com/DataDog/zstd"
	"google.golang.org/grpc/encoding"
	"io"
)

// in case of type check
var _ encoding.Compressor = (*compressor)(nil)

// Name is the name registered for the zstd compressor.
const Name = "zstd"

func init() {
	c := &compressor{}
	encoding.RegisterCompressor(c)
}

type compressor struct{}

// We don't need to use pool, because the underlying is based on c language
// which will help us to handle memory management
func (c *compressor) Compress(w io.Writer) (io.WriteCloser, error) {
	nw := zstd.NewWriter(w)
	if nw != nil {
		return nw, nil
	} else {
		return nil, errors.New("unable to new zstd writer")
	}
}

// We don't need to use pool, because the underlying is based on c language
// which will help us to handle memory management
func (c *compressor) Decompress(r io.Reader) (io.Reader, error) {
	nr := zstd.NewReader(r)
	if nr != nil {
		return nr, nil
	} else {
		return nil, errors.New("unable to new zstd reader")
	}
}

func (c *compressor) Name() string {
	return Name
}
