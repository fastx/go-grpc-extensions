package zstd

import (
	"bytes"
	"fmt"
	"github.com/valyala/gozstd"
	"google.golang.org/grpc/encoding"
	"testing"
)

var str1 = []byte("dfdasfdsafdsaf12323dfasdfdsafadfdsafdsafdgsdfgfrewtewdfgdg1232323re34565463233asfdsadfdfd1232323232222222222222222222435643564326465767676")
var str2 = []byte(`testify提供了suite包提供了类似rails minitest中可以给每个测试用例进行前置操作和后置操作的功能，这个方便的功能，在前置操作和后置操作中去初始化和清空数据库，就可以帮助我们实现第一个目标。
同时，还可以声明在这个测试用例周期内都有效的全局变量
作者：gamefu_dl
链接：https://www.jianshu.com/p/720b15485fd0
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`)

func TestCompress1(t *testing.T) {
	var cmp = encoding.GetCompressor(Name)
	test1(cmp)
	test2(cmp)
	fmt.Printf("done")
}

func test1(cmp encoding.Compressor) {
	var buf bytes.Buffer
	z, _ := cmp.Compress(&buf)
	_, e := z.Write(str1)
	if e != nil {
		fmt.Printf("err: %s\n", e)
	}

	if err := z.Close(); err != nil {
		fmt.Printf("err: %s\n", err)
	}

	compressData := buf.Bytes()

	rd := bytes.NewReader(compressData)
	d, _ := cmp.Decompress(rd)

	bufx := bytes.NewBuffer(nil)
	n, _ := bufx.ReadFrom(d)
	fmt.Printf("d: %d, s: %s\n", n, string(bufx.Bytes()))
}

func test2(cmp encoding.Compressor) {
	var buf bytes.Buffer
	z, _ := cmp.Compress(&buf)
	_, e := z.Write(str2)
	if e != nil {
		fmt.Printf("err: %s\n", e)
	}

	if err := z.Close(); err != nil {
		fmt.Printf("err: %s\n", err)
	}

	compressData := buf.Bytes()
	//size := len(compressData)

	rd := bytes.NewReader(compressData)
	d, _ := cmp.Decompress(rd)

	bufx := bytes.NewBuffer(nil)
	n, _ := bufx.ReadFrom(d)
	fmt.Printf("d: %d, s: %s\n", n, string(bufx.Bytes()))
}

func TestCompress2(t *testing.T) {
	str := []byte("dfdasfdsafdsaf12323dfasdfdsafadfdsafdsafdgsdfgfrewtewdfgdg1232323re34565463233asfdsadfdfd1232323232222222222222222222435643564326465767676")
	enbytes := gozstd.Compress(nil, str)

	debytes, _ := gozstd.Decompress(nil, enbytes)

	fmt.Printf("result: %s\n", string(debytes))
}
