package gozstd

import (
	"errors"
	"fmt"
	"github.com/valyala/gozstd"
	"google.golang.org/grpc/encoding"
	"io"
	"io/ioutil"
	"sync"
)

// in case of type check
var _ encoding.Compressor = (*compressor)(nil)

// Name is the name registered for the gozstd compressor.
const Name = "zstd"

const compressionLevel = gozstd.DefaultCompressionLevel
const (
	MinCompressionLevel = 1
	MaxCompressionLevel = 19
)

func init() {
	c := &compressor{}
	c.poolCompressor.New = func() interface{} {
		return &writer{Writer: gozstd.NewWriterLevel(ioutil.Discard, compressionLevel), pool: &c.poolCompressor}
	}
	encoding.RegisterCompressor(c)
}

func SetLevel(level int) error {
	if level < MinCompressionLevel || level > MaxCompressionLevel {
		return fmt.Errorf("grpc: invalid zstd compression level: %d", level)
	}
	c := encoding.GetCompressor(Name).(*compressor)
	c.poolCompressor.New = func() interface{} {
		w := gozstd.NewWriterLevel(ioutil.Discard, level)
		return &writer{Writer: w, pool: &c.poolCompressor}
	}
	return nil
}

func (c *compressor) Compress(w io.Writer) (io.WriteCloser, error) {
	z := c.poolCompressor.Get().(*writer)
	z.Writer.Reset(w, nil, compressionLevel)
	return z, nil
}

func (c *compressor) Decompress(r io.Reader) (io.Reader, error) {
	z, inPool := c.poolDecompressor.Get().(*reader)
	if !inPool {
		newRd := gozstd.NewReader(r)
		if newRd == nil {
			return nil, errors.New("unable to new gozstd reader")
		}
		return &reader{Reader: newRd, pool: &c.poolDecompressor}, nil
	}
	z.Reset(r, nil)
	return z, nil
}

func (c *compressor) Name() string {
	return Name
}

type writer struct {
	*gozstd.Writer
	pool *sync.Pool
}

func (z *writer) Close() error {
	defer z.pool.Put(z)
	return z.Writer.Close()
}

type reader struct {
	*gozstd.Reader
	pool *sync.Pool
}

func (z *reader) Read(p []byte) (n int, err error) {
	n, err = z.Reader.Read(p)
	if err == io.EOF {
		z.pool.Put(z)
	}
	return n, err
}

type compressor struct {
	poolCompressor   sync.Pool
	poolDecompressor sync.Pool
}
