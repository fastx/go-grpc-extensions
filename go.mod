module gitlab.com/fastx/go-grpc-extensions

go 1.13

require (
	github.com/DataDog/zstd v1.4.4
	github.com/golang/snappy v0.0.1
	github.com/valyala/gozstd v1.6.3
	google.golang.org/grpc v1.25.1
)
